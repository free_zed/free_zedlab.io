Title: Je suis nul·le !
Date: 2023-02-18 10:35
Summary: SUMMARY
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, réparation, électroménager, méthode
Status: published

Par **[Guillaume Ayoub][author]** - Salle [Henri Poincaré][hpoincare]


### [Je suis nul·le !][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Je ne sais rien faire de vraiment utile. Toutes les personnes que je côtoie sont bien meilleures que moi. C’est pour cela que je n’ai rien à dire d’intéressant, et encore moins le talent pour faire une conférence. Si vous avez le sentiment d’être un peu comme moi, venez m’aider !

_[Support][support]_

---

Notes personnelles
==================

* présentation trop rapide
* je suis nul dans pleins de situations
    - chercher des financement pour un logiciel libre
        1. **ce que je fais ne sert a rien**
            * trop compliqué
            * ça n'à aucune utilité pour la société
            * ma famille croit que je répare dis fours à micro-onde
        1. **je n'ai aucune légitimité**
            * mes diplômes sont insufisant
            * trop peu d'expérience
        1. **je n'ai jamais rien inventé**
            * je n'ai jamais eu une idée révolutionnaire
            * mon code est banal
            * ma vie estune suite de lecture de de tuto et de doc
        1. **la terre entière est meilleure que moi**
    - Conclusion alernative…

[abstract]: https://www.pycon.fr/2023/fr/talks/30m.html#je-suis-nul-le
[author]: https://
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://


[cdarwin]: https://fr.wikipedia.org/wiki/Charles_Darwin
[rfranklin]: https://fr.wikipedia.org/wiki/Rosalind_Franklin
[tedison]: https://fr.wikipedia.org/wiki/Thomas_Edison
[hpoincare]: https://fr.wikipedia.org/wiki/Henri_Poincaré
[awegener]: https://fr.wikipedia.org/wiki/Alfred_Wegener
