Title: Clôture du dimanche
Date: 2023-02-19 15:47
Summary: Session de clôture de la PyConFR 2023.
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, keynote
Status: published

Par **[Marc Debureaux][mdebureaux]** - Salle [Alfred Wegener][awegener]

### [Clôture du dimanche][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Session de clôture de la PyConFR 2023.

---

Notes personnelles
==================

* Remerciements
* Venez nous voir: afpy.org


[abstract]: https://www.pycon.fr/2023/fr/talks/
[awegener]: https://fr.wikipedia.org/wiki/Alfred_Wegener
[mdebureaux]: https://fr.linkedin.com/in/mdebnet
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://
