Title: Molecule, testeur complet de vos besoins Ansibles
Date: 2020-02-21 19:00
Category: Bloc-notes
Status: published
Summary: Présentation de l'outils [Molecule][molecule] et retour d'expérience
Tags: talk, lyon, ansible, molecule, dev, devops, cd, ci
Translation: false
Lang: fr

Par [Thibault Lecoq][1] & [Quentin Le Baron][2], organisé par [Café Devops][cafedevops] (via [Meetup][meetup]). Archive sur [Café Devops][support].

_Prérequis_ : Avoir manipulé Ansible et les rôle Ansible.

Le sujet de cette session sera de présenter dans une première partie l'outil [Molecule][molecule], pourquoi et comment l'utiliser pour tester du code ansible (rôles, playbooks, modules, filters).

Une démonstration de l'usage de molecule sur un role ansible viendra cloturer cette première partie.

La deuxième partie sera axés sur notre retour d'expérience de comment nous avons intégré molecule dans l'écosystème du projet linky sur lequel nous travaillons aujourd'hui.

Nous verrons pourquoi nous avons choisi d'intégrer molecule, les avantages que cela apporte et en quoi est-ce un investissement rentable. S'en suivra une présentation de l'architecture choisi ainsi qu'une explication détaillé de sa mise en place de A à Z.
Nous terminerons par une démonstration présentant point par point comment mettre en place cette architecure et intégrer dans celle-ci molecule à un rôle ansible.

1er partie [Molecule][molecule]
- Qu'est ce que c'est et pourquoi l'utiliser ?
- Comment cela fonctionne et comment l'utiliser
- Demonstration sur un role ansible

2eme partie Intégration continue de [Molecule][molecule]
- Contexte : explication du besoin
- Présentation de l'architecture : Jenkins + Gitea + AWS
- Explication détaillé du fonctionnement (pipeline, webhook ...)
- Demonstration point par point de l'intégration de molecule

[Thibault Lecoq][1] & [Quentin Le Baron][2] Intégrateur DevOps sur l'un des projets Linky.

---

Notes personnelles:

* outils python
* monte un environnement temporaire dédié au test
    * utilisable en local comme sur infra
* tout type de test


Comment ça marche ?
-----------------
* Linux
* internet
* `ansible==2.8`
* `molecule==2.20`
* plugin : docker CE / AWS EC2 / OpenStack / ?
* test (python) avec [`Testinfra`][3]

**Marmo** (bientôt _open-sourced_ sur le _Github_ de [Thibault Lecoq][1])

* ajoute un scenario molecule dans le rorle ansible a tester
* structure alternative possible : lisibilité & DRY

Tous les détails dans le [support de présentatiion][pdf]


[1]: https://www.linkedin.com/in/thibault-lecoq-0a550214a
[2]: https://github.com/kuty22
[3]: https://testinfra.readthedocs.io/en/latest
[ansible]: https://www.ansible.com/
[cafedevops]: https://cafedevops.org/
[meetup]: https://www.meetup.com/fr-FR/cafe-devops-lyon/events/268510842/
[molecule]: https://github.com/ansible-community/molecule
[repo]: https://gitlab.com/free_zed/
[support]: https://cafedevops.org/posts/molecule-full-tester-of-your-ansible-needs/
[pdf]: https://cafedevops.org/pdf/Molecule_full_tester_of_your_need.pdf
