Title: Session d'ouverture
Date: 2023-02-18 09:22
Summary: Session d'ouverture de PyConFr 2023
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, keynote
Status: published

Par **[Marc Debureaux][mdebureaux]** - Salle [Alfred Wegener][awegener]


### [Session d'ouverture][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Session d'ouverture de PyConFr 2023.

---

Notes personnelles
==================

* streaming: [raffut_media](https://www.raffut.media/) sur [Peer Tube](https://indymotion.fr/c/raffut_media/videos)
* tours des [sponsors](https://www.pycon.fr/2023/fr/sponsors.html)


[abstract]: https://www.pycon.fr/2023/fr/talks/plenary.html#accueil-welcome
[awegener]: https://fr.wikipedia.org/wiki/Alfred_Wegener
[mdebureaux]: https://fr.linkedin.com/in/mdebnet
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
