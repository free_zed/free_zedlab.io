Date: 2023-02-02 11:00
Category: Bloc-notes
Status: published
Summary: Entraîner un modèle c'est bien, l'utiliser c'est mieux
Tags: vtt, ovh, talk, paris, ml, process, python, dev, numerisation
Title: Data, IA... et si c'était la solution pour comprendre la langue des signes ?


Par [Elea Petton][elea] & [Claire Gallot][claire] - [**DEMO** _Very Tech Trip_][vtt]

> Dans le domaine de l'Intelligence Artificielle, nous parlons souvent de Machine Learning ou de Deep Learning. Mais au final que signifient ces termes et surtout comment peuvent-ils nous aider dans la vie courante ?
> Durant ce talk nous allons tout d'abord tenter de comprendre la place qu'occupe l'IA dans le vaste domaine de la santé. Nous montrerons ensuite l'importance de la donnée et nous développerons une app d'IA capable de détecter et d'interpréter le langage des signes. Ce talk permettra de montrer comment il est possible de traiter la donnée brute rapidement pour l'adapter un algorithme d'IA grâce à Apache Spark. Nous verrons aussi comment utiliser le traitement d'image distribué comme moyen d'enrichir ses données d'entraînement. La seconde étape consistera à entraîner un modèle de détection d'objets à reconnaître les différents signes de l'alphabet ASL (American Sign Language) au travers d'un notebook Jupyter.
>
> Entraîner un modèle c'est bien, l'utiliser c'est mieux ! Les développeurs et développeuses apprendront à créer une app d'intelligence artificielle à partir du modèle entraîné pour de la détection de signes sur images ou flux vidéos. Pour finir, nous verrons comment déployer cette application avec Docker pour pouvoir la rendre accessible. Cette application permettra à quiconque de comprendre le langage des signes avec une détection et une retranscription écrite.

---

Notes personnelles
==================

- Claire: Data convergeance team
- Éléa: ML engineer
- Objectif: interpréter le langage des signes
    * IA au service de l'accessibilité
- dataset de la langue des signe libre de droit (USA)
    - 1728 images
    - 26 classes
    - photo de petite taille centré sur les main
* Réussir a détecter les mains
    - algo YOLOv7: You Only Look Once: algo mono passage
    - live demo: V pas très fiable, O mieux
    - => résultats faibles
    - DB trop faible
    - sous représentation: coleurs de peau, présence de tatouage/bijou, fond uniforme, pas de main d'enfants
* Vers un meilleurs modèle
    - augmenter de dataset: un nouveau ou améliorer le dataset existant
    - améliorer le dataset existant
        * image flip: représentation des gauchers
        * image shift
        * changer la luminosité, saturation, blur, etc.
        * ->[`albumentation`]() lib python
            - 3h pour augmenter le dataset x3
        * utiliser du calcul distribué [`apache spark`]() => 3min de temps de traitement
* _plus le temps de suivre,

[claire]: https://www.linkedin.com/in/claire-gallot-b80b72153/
[elea]: https://www.linkedin.com/in/elea-petton
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
