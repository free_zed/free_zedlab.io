Title: Ceph, le stockage du futur
Date: 2022-04-03 12:00
Summary: Présentation de Ceph, une plateforme libre de stockage distribué.
Category: Bloc-notes
Tags: talk, jdll, lyon, storage, ceph
Status: Published
Lang: fr

### [Ceph, le stockage du futur](https://pretalx.jdll.org/jdll2022/talk/SBZZGF/)

par [Gregory Colpart](http://www.gcolpart.com/) - 2022-04-03 12:00–12:55 Salle des cultures

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

Présentation de [Ceph][ceph], une plateforme libre de stockage distribué.

[Ceph][ceph] est une plateforme libre de stockage distribué. [Ceph][ceph] permet de stocker des objets répartis sur plusieurs nœuds de façon redondante. Cela permet de multiples cas d'usage : archiver de grand volume de données (Po), partager des fichiers entre serveurs… mais surtout cela devient un outil de base des solutions libres de virtualisation/conteneurisation (KVM, Proxmox, OpenStack, Kubernetes…) et des solutions de « Cloud Storage » chez DigitalOcean, OVH, etc. Comment fonctionne Ceph en interne ? Revenons sur les mécanismes de base de la technologie : OSD, PG, monitor, etc. Comment utiliser Ceph concrètement ? Parlons de l'installation et l'utilisation en mode bloc (rdb) ou en mode filesystem (CephFS).

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]

---

Notes personnelles:

- gérant [evolix](https://evolix.com/presentation.html)
    - infogérant, hébergement, HA,
- Ceph = céphalopode
    - historique stockage:
        - main préhistorique, écriture mésopotamien, métiers a tisser, HDD, baie stockage
        - NDB
        - DRBD
        - LVM
        - ZFS, BTRFS
        - GFS, Lustre, GlusterFS, ...
    - disque distribué auto-réparant
    - historique Ceph:
        - [Sage Weil][sage_weil]
                - co-fondateur [DreamHost][dreamhost] en 97
                - these [_Ceph: Reliable, Scalable, and High-Performance Distributed Storage_][these]
                - co-fondateur [Inktank Storage][inktank] en
                        - racheté par RedHat en 2014
    - LGPL / github / Linux / freeBSD
    - Concept
        - fiable, pas de SPOF, zero downtime
        - extensible
    - cas d'usage
        - CERN big bang III
        - gros volumes
        - partage multi-serveur
        - stockage objet compatible S3
    - **OSD**: _Object Storage Device_
    - **PG**: _Placement Group_
        - 128 a 1024 PGs par pool
        - PG repartit sur 3 OSD
    - Pool
        - plusieurs pool par cluster
        - 1 pool par usage
        - 1 pool par type de disque (SSD, HDD, ...)
        - mode réplication ou _erasure coding_
    - Accès
        - _file_: cephFS
        - _block_
        - _object_ (compatible S3)
    - Comment ça marche
        - des démons

[captation]: https://www.videos-libr.es/w/8eh1ikxAD2PBNWjB6ew8gV
[ceph]: https://ceph.io/en/
[sage_weil]: https://en.wikipedia.org/wiki/Sage_Weil
[dreamhost]: https://www.dreamhost.com/
[these]: https://ceph.io/assets/pdfs/weil-thesis.pdf
[inktank]: https://en.wikipedia.org/wiki/Inktank_Storage
