Title: L’enfer du packaging Python
Category: Bloc-notes
Date: 2022-05-18 19:00
Status: published
Summary: Mais pourquoi diable est-ce l’enfer de créer et partager des paquets Python ? 
Tags: talk, lyon, afpy, dev, packaging, python


Par [Guillaume Ayoub][author] (<sup>[1][authorgl], [2][authorgh]</sup>), organisé par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy]. (via [Meetup][meetup]).

_**Support**: sur [`courtbouillon.org`][support] ou [archive][support-local]_

> Ce mois-ci, Guillaume vient nous parler de packaging Python !
>
> 🔥🎁🔥
>
> Écrire du code en Python, c’est sympa. Vous êtes là, vous devez donc être un petit peu d’accord avec cela. Mais si, passée votre timidité, vous avez le courage de partager votre code avec les autres, alors vous voilà face à un problème de taille : le packaging. Vous cherchez tant bien que mal la documentation associée, vous trouvez nombre d’informations contradictoires, vous finissez après des heures d’effort sur d’obscurs forums au fin fond de la toile… 😢
>
> 👿 Mais pourquoi diable est-ce l’enfer de créer et partager des paquets Python ? 👿
>
> Nous parlerons durant cette présentation de l’histoire du packaging Python, des nombreux problèmes rencontrés au fil de son existence, et des manières récentes et agréables de faire des paquets avec le sourire. 😁

---

Notes personnelles
==================

- la méthode: usage des wheels
    * code source => wheel (zip)
- PEP:
    * [PEP 427 – The Wheel Binary Package Format 1.0][pep-0427]
    * [PEP 517 – A build-system independent format for source trees (architecture des pacquets)][pep-0517]
    * [PEP 518 – Specifying Minimum Build System Requirements for Python Projects (`pyproject.toml`)][pep-0518]
    * [PEP 621 – Storing project metadata in `pyproject.toml`][pep-0621]
    * [PEP 660 – Editable installs for pyproject.toml based builds (wheel based) (intallation éditable)][pep-0660]
- outillage de base
    * adieu `distutils`
    * `setuptools` pour quelques rares cas particuliers
        - `wheel` (pacquet)
    * `build` peut utiliser des constructeurs de wheel
    * `pip`
    * WIP: `installer` (lire la wheel) hors BS, pour se passer
    * `installer`
    * `twine` pour publier sur PyPI
- Outillage avancé
    * `flit` moins vaste (juste des wheels) utilise tout les fichiers versionnés
    * `poetry` très vaste
    * `…
- Condition un `pip` récent

**Bonus :** Origine du nom _wheel_

* [Where the name “Wheel” comes from? - python.org](https://discuss.python.org/t/where-the-name-wheel-comes-from/6708)
* [Cheese Shop sketch](https://en.wikipedia.org/wiki/Cheese_Shop_sketch)


[afpy]: https://www.afpy.org/
[authorgh]: https://github.com/liZe
[authorgl]: https://www.afpy.org/
[author]: https://yabz.fr/
[cbouillon]: https://www.courtbouillon.org/
[ghostscript]: https://www.ghostscript.com/
[meetup]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/events/285563922/
[pep-0427]: https://peps.python.org/pep-0427/
[pep-0517]: https://peps.python.org/pep-0517/
[pep-0518]: https://peps.python.org/pep-0518/
[pep-0621]: https://peps.python.org/pep-0621/
[pep-0660]: https://peps.python.org/pep-0660/
[stella]: https://stella.coop/
[support]: https://www.courtbouillon.org/static/presentations/L%E2%80%99enfer%20du%20packaging.pdf
[support-local]: {attach}/pdf/20220518-afpy-lyon-enfer_packaging-python.pdf
