Title: IFTTD #14.exe - Coder peu, coder mieux
Date: 2021-05-14 22:51
Summary: Participation au résumé de l'épisode #14 du podcast IFTTD
Category: Conférences
Tags: talk, podcast, agile, méthode, collectif, extrem-programming, ifttd, git, dev, performance,
Status: Published

J'ai eu le plaisir de participer au _compilé_ de l'épisode #14 du podcast _If This Then Dev_.

* La page de ce _compilé_ : _[#14.exe vu par Frederic Zind - Coder peu, coder mieux](https://art19.com/shows/ifttd-if-this-then-dev/episodes/fbc2a15a-40c2-4b86-a5a5-8eed914c871b)_
* Le site du podcast **IFTTD** : [`ifttd.io`](https://ifttd.io)
* La page de l'épisode #14 : _[Dimitri Baeli : Coder peu, coder mieux](https://ifttd.io/14-coder-peu-coder-mieux-dimitri-baeli/)_
