Title: Au bonheur des likes
Date: 2019-11-02 18:00
Summary: Biais cognitifs & science comportementale au service du capitalisme de l’attention
Category: Bloc-notes
Tags: pyconfr, talk, business, cognition, comportement, web, bordeaux
Status: published

### [Plénière n°2 / Keynote #2: Au bonheur des likes][1]

Par [Nina Cercy][2] − Salle [Alfred Wegener][awegener] − Samedi à 17 h 30

![logo PyConFr Bordeaux 2019][pyconimg]
Biais cognitifs, sciences comportementales, incitations douces : le cerveau des utilisateurs/trices est devenu notre terrain de jeu — et l’infantilisation notre meilleur business model.

Face aux hochets attentionnels et aux biberons de dopamine, notre capacité à proposer un numérique adulte, émancipateur est la victime collatérale du capitalisme de l’attention. Ne jetons pas le bébé avec l’eau du bain : la science comportementale, c’est aussi le remède dans le mal.

---

Notes personnelles:

Arrivé en retard… Conf a revoir en vidéo.

À montrer aux utilisateurs des services.

Piste émancipatrice individuelles proposée :

- anticipation
- minimisation
- autonome
- évaluation

Pas de piste collectives.

[1]: https://www.pycon.fr/2019/fr/talks/keynote.html#pl%C3%A9ni%C3%A8re%20n%C2%B02%20%2F%20keynote%20%232%3A%20au%20bonheur%20des%20likes
[2]: https://fr.linkedin.com/in/ninacercy/en
[awegener]: https://en.wikipedia.org/wiki/Alfred_Wegener
[pyconimg]: {static}/img/250-pycon-fr19.png
