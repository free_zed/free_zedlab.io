Title: Accompagnement émancipateur au numérique
Date: 2022-04-03 13:00
Summary: Réflexions pour un accompagnement au numérique qui fasse sens.
Category: Bloc-notes
Tags: talk, jdll, lyon, logiciel libre, collectif, citoyen
Status: Published
Lang: fr

### [Qu'est-ce que c'est, un accompagnement émancipateur au numérique ?](https://pretalx.jdll.org/jdll2022/talk/HLGAA8/)

par [Julie / Romain](https://letab.li/) - 2022-04-03 13:00–13:55, Vie citoyenne

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

Réflexions pour un accompagnement au numérique qui fasse sens.

Comment accompagner au mieux un large public dans ses choix d'outils et d'usages numériques ? Comment ne pas proposer des solutions toutes faites ? Le logiciel libre est-il toujours la meilleure solution ? Comment produire des changements durables ? À partir de retours d'expérience de formations, ateliers, cryptoparties ou install partys, on vous propose des axes de réflexion collective.

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]

---

Notes personnelles:

- C'est quoi un accompagnement émancipateur?
    - Se passer des accompagnateurs
    - choix fait par la personne forme
    - prendre en compte les besoin et contrainte
- Partir de la personne, voir travail de [Karl Rogers](https://fr.wikipedia.org/wiki/Carl_Rogers)
- Sortir de la position d'experte ([Jacques Ranciere - la nuit des prolétaires](https://www.fayard.fr/pluriel/la-nuit-des-proletaires-9782818502969))
    - a éviter: "Qui ne connais pas?" / "Tout le monde connaît!"


[captation]: https://www.videos-libr.es/w/q1XEHTex9TspGZ3p37dhGG
