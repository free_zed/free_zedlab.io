Title: Booster Python avec Rust : le cas de Mercurial
Date: 2019-11-03 15:00
Summary: Nous avons découvert des problèmes liés l'interaction de ces deux langages et trouvé des solutions qui n'avaient jusque-là - à notre connaissance - jamais été publiées
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, rust, dev, mercurial, c, performance, python
Status: published

### [Booster Python avec Rust : le cas de Mercurial][1]

Par [Raphaël Gomès][4] − Salle [Charles Darwin][cdarwin] − Dimanche à 14 h 30

![logo PyConFr Bordeaux 2019][pyconimg]
Historiquement, le gestionnaire de version Mercurial utilise des modules en C pour améliorer ses performance lorsque Python ne suffit plus. L'année dernière, des contributeurs ont commencé à utiliser des modules en Rust comme une alternative plus sécuritaire, plus agréable et mieux outillée que C afin de compléter la base Python de Mercurial.

Depuis fin 2018, Octobus a le plus grand nombre de contributions Rust dans Mercurial. Nous avons découvert des problèmes liés l'interaction de ces deux langages et trouvé des solutions qui n'avaient jusque-là - à notre connaissance - jamais été publiées sur Internet.

Au programme :

*   Une vue d'ensemble de l'intégration de Rust à Python
*   Les points positifs et négatifs de notre solution et les alternatives
*   Des chiffres comparatifs de performance
*   Une porte ouverte à la curiosité quant aux détails excessivement techniques.

---

Notes personnelles:

Arrivé en retard… Conf a revoir en vidéo.

[Octobus][2]

[Mercurial][3]

Rust permet d'améliorer les perf de python, des choses encore a faire, mais python est encore très pertinent parce que très lisible, très rapide à mettre en œuvre :

> Python est plus rapide que du code Rust _pas fini d'être écrit_.

[1]: https://www.pycon.fr/2019/fr/talks/conference.html#booster%20python%20avec%20rust%E2%80%AF%3A%20le%20cas%20de%20mercurial
[2]: https://octobus.net/
[3]: https://fr.wikipedia.org/wiki/Mercurial
[4]: https://raphaelgomes.dev
[cdarwin]: https://fr.wikipedia.org/wiki/Charles_Darwin
[pyconimg]: {static}/img/250-pycon-fr19.png
