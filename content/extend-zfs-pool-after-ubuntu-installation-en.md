Title: Extend a ZFS root pool after Ubuntu installation
Date: 2021-01-03 21:55
Modified: 2021-04-30
Summary: 1st step after installing Ubuntu on ZFS
Category: Bloc-notes
Tags: zfs, ubuntu, admin, shell, cli, performance, logiciel libre,
Status: Published
Translation: true
Lang: en
Slug: extend-zfs-root-pool-after-ubuntu-installation

Since [Eoan Ermine (Ubuntu 19.10)](https://ubuntu.com/blog/enhancing-our-zfs-support-on-ubuntu-19-10-an-introduction), Ubuntu installer (for desktop) can use [ZFS](https://github.com/openzfs/zfs) as root file-system.

The current LTS, [Focal Fossa (Ubuntu 20.04)](https://ubuntu.com/blog/zfs-focus-on-ubuntu-20-04-lts-whats-new) provides only a mono-disk installation, here's how to enhance a mono-disk installation into a mirrored-disk installation to use data resilience provided by ZFS.

🚧 _ZFS on root is still experimental_ 🚧


🔧 Howto
--------

* `sda` is the disk used by installer
* `sdb` is the disk we're going to add to create a mirror

```
user@focal:~$ lsblk -a
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931,5G  0 disk
├─sda1   8:1    0   513M  0 part
├─sda2   8:2    0     1K  0 part
├─sda5   8:5    0     2G  0 part [SWAP]
├─sda6   8:6    0     2G  0 part
└─sda7   8:7    0   927G  0 part
sdb      8:16   0 931,5G  0 disk
```

```bash
user@focal:~$ blkid
/dev/sda1: UUID="BFE4-2413" TYPE="vfat" PARTUUID="f0771fca-01"
/dev/sda5: UUID="42456868-afc5-4dea-8b73-11257b84890e" TYPE="swap" PARTUUID="f0771fca-05"
/dev/sda6: LABEL="bpool" UUID="11578277722432725130" UUID_SUB="9520999198366150859" TYPE="zfs_member" PARTUUID="f0771fca-06"
/dev/sda7: LABEL="rpool" UUID="13693210375688435814" UUID_SUB="8709443765040685487" TYPE="zfs_member" PARTUUID="f0771fca-07"
```

Ubuntu creates 2 [`pools`](https://www.freebsd.org/doc/handbook/zfs-term.html#zfs-term-pool) :

1. `bpool` : containing boot _pool_
1. `rpool` : containing root _pool_, with all file-system

```bash
user@focal:~$ zpool list
NAME    SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
bpool  1,88G  91,2M  1,79G        -         -     0%     4%  1.00x    ONLINE  -
rpool   920G  3,19G   917G        -         -     0%     0%  1.00x    ONLINE  -
```

```bash
user@focal:~$ zpool status
  pool: bpool
 state: ONLINE
  scan: none requested
config:

	NAME           STATE     READ WRITE CKSUM
	bpool          ONLINE       0     0     0
	  f0771fca-06  ONLINE       0     0     0

errors: No known data errors

  pool: rpool
 state: ONLINE
  scan: none requested
config:

	NAME           STATE     READ WRITE CKSUM
	rpool          ONLINE       0     0     0
	  f0771fca-07  ONLINE       0     0     0
```

Run `fdisk` to copy `sda` partition table to `sdb` using sfdisk compatible script files ([_see manpage_](https://manpages.ubuntu.com/manpages/focal/en/man8/fdisk.8.html#script%20files)).

```bash
user@focal:~$ fdisk /dev/sda #copy to file with O
user@focal:~$ fdisk /dev/sdb #import from file with I
```

```bash
user@focal:~$ lsblk -a
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931,5G  0 disk
├─sda1   8:1    0   513M  0 part
├─sda2   8:2    0     1K  0 part
├─sda5   8:5    0     2G  0 part [SWAP]
├─sda6   8:6    0     2G  0 part
└─sda7   8:7    0   927G  0 part
sdb      8:16   0 931,5G  0 disk
├─sdb1   8:17   0   513M  0 part
├─sdb2   8:18   0     1K  0 part
├─sdb5   8:21   0     2G  0 part
├─sdb6   8:22   0     2G  0 part
└─sdb7   8:23   0   927G  0 part
```

[Attach](https://www.freebsd.org/doc/handbook/zfs-zpool.html#zfs-zpool-attach) the twin partitions on pools :

```bash
user@focal:~$ sudo zpool attach bpool f0771fca-06 /dev/sdb6
user@focal:~$ sudo zpool attach rpool f0771fca-07 /dev/sdb7
```
🎉 Tada!

```bash
user@focal:~$ zpool status
  pool: bpool
 state: ONLINE
  scan: resilvered 92,2M in 0 days 00:00:01 with 0 errors on Sun Jan  3 22:22:09 2021
config:

	NAME             STATE     READ WRITE CKSUM
	bpool            ONLINE       0     0     0
	  mirror-0       ONLINE       0     0     0
	    f0771fca-06  ONLINE       0     0     0
	    sdb6         ONLINE       0     0     0

errors: No known data errors

  pool: rpool
 state: ONLINE
  scan: resilvered 4,05G in 0 days 00:01:06 with 0 errors on Sun Jan  3 22:24:05 2021
remove: Removal of vdev 1 copied 3,36M in 0h0m, completed on Sun Jan  3 21:14:16 2021
    456 memory used for removed device mappings
config:

	NAME             STATE     READ WRITE CKSUM
	rpool            ONLINE       0     0     0
	  mirror-0       ONLINE       0     0     0
	    f0771fca-07  ONLINE       0     0     0
	    sdb7         ONLINE       0     0     0

errors: No known data errors
```

📝 30/4/2021 edit
-----------------

1. Steps compatible with [version `21.04` «_Hirsute Hippo_»](https://discourse.ubuntu.com/t/hirsute-hippo-release-notes/19221) instaler;
    * the partition numbering seems to be continuous now
1. Enlarge `swap` with the available partition on the added disk

```bash
user@hirsute:~$ free -h
              total       utilisé      libre     partagé tamp/cache   disponible
Mem:           11Gi       974Mi        10Gi       7,0Mi       259Mi        10Gi
Swap:          2,0Gi      0B           2,0Gi

user@hirsute:~$ sudo mkswap /dev/sdb5
Configure l'espace d'échange (swap) en version 1, taille = 2 GiB (2147479552 octets)
pas d'étiquette, UUID=bbbbbbbb-bbbbb-bbbb-bbbb-bbbbbbb

user@hirsute:~$ grep swap /etc/fstab
UUID=aaaaaaaa-aaaaa-aaaa-aaaa-aaaaaaa	none	swap	sw	0	0
UUID=bbbbbbbb-bbbbb-bbbb-bbbb-bbbbbbb	none	swap	sw	0	0

user@hirsute:~$ sudo swapon -U bbbbbbbb-bbbbb-bbbb-bbbb-bbbbbbb

user@hirsute:~$ free -h
              total       utilisé      libre     partagé tamp/cache   disponible
Mem:           11Gi       1,0Gi        10Gi       7,0Mi       261Mi        10Gi
Swap:          4,0Gi      0B           4,0Gi
```

---

🔖 References
-------------


1. [_ZFS Features and Terminology_](https://www.freebsd.org/doc/handbook/zfs-term.html) - `www.freebsd.org`
1. [_The Z File System (ZFS)_](https://www.freebsd.org/doc/handbook/zfs-quickstart.html) - `www.freebsd.org`
1. [_OpenZFS_](https://github.com/openzfs/zfs) - `github.com`
1. [_Enhancing our ZFS support on Ubuntu 19.10 – an introduction_](https://ubuntu.com/blog/enhancing-our-zfs-support-on-ubuntu-19-10-an-introduction) - `ubuntu.com`
1. [_ZFS focus on Ubuntu 20.04 LTS: what’s new?_](https://ubuntu.com/blog/zfs-focus-on-ubuntu-20-04-lts-whats-new) - `ubuntu.com`
1. [_zpool manpage_](https://manpages.ubuntu.com/manpages/focal/en/man8/zpool.8.html) - `manpages.ubuntu.com`
1. [_zfs manpage_](https://manpages.ubuntu.com/manpages/focal/en/man8/zfs.8.html) - `manpages.ubuntu.com`
