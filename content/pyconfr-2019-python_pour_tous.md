Title: Python pour tous 
Date: 2019-11-02 16:00
Summary: Des ateliers dans des universités à destination de collégiens et lycéens
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, dev, pygame, logiciel libre, citoyen, django, python
Status: published

### [Python pour tous : amener un zeste de programmation dans les milieux moins favorisés][1]

Par [Nathanaël Langlois][6], [Amaury Carrade][5], Titouan Soulard − Salle [Rosalind Franklin][rfranklin] − Samedi à 15 h 00


![logo PyConFr Bordeaux 2019][pyconimg]
Python, aussi bien utilisé pour du développement d’application que pour des programmes de calculs complexe, ouvre par sa facilité de prise en main un champ gigantesque de possibilité à toutes les populations. L’association “Zeste de Savoir”, fédérée autour d’un site communautaire de partage de connaissances éponyme, organise régulièrement des ateliers dans des universités à destination de collégiens et lycéens en proposant notamment aux enfants de quartiers défavorisés de s’y joindre. Comment rendre la programmation simple et attractive ? C'est ce à quoi nous tentons de répondre dans nos ateliers et cette conférence.

---

Notes personnelles:

[Zeste de savoir][3] : asso _fork_ du SdZ/SimpleIT (? SdZ // ZdS)

Fondé en 2014 / +12000 membres

Plateforme en Django

Zeste de code : passer IRL

1er atelier sponsorisé par [Meet and code][2]

Construction d'un jeu de type _snake_ sur un atelier de 4h

Mise en place d'objectifs :

1. Afficher le jeu & serpent
    * Une bibliothèque d'abstraction pour faciliter le dev et traduire partielement
1. Déplacer le serpent
1.  ?
1. Gestion des évènements

Pédagogie : une heure de rudiment technique

Initiation au monde réel de la programmation: documentation de la bibliothèque d'abstraction

Débogage avec les collision du serpent

Ajout de bonus pour les plus avancé: (marche arrière, _coin_ du serpent, )

Besoin d'essaimer partout ces modèles

Environnement de dev _[Spyder][4]_

Matériel informatique fourni par le lieu

Cible jeune lié aux début avec [Meet and code][2], mais autres publiques possibles

Dans les collège et lycée pub pour l'évènements

Continuité de la formation pour les intéressés : via le forum/site

Combien de session de 4h : une seule


[1]: https://www.pycon.fr/2019/fr/talks/conference.html#python%20pour%20tous%E2%80%AF%3A%20amener%20un%20zeste%20de%20programmation%20dans%20les%20milieux%20moins%20favoris%C3%A9s
[2]: https://meet-and-code.org/
[3]: https://github.com/zestedesavoir
[4]: https://www.spyder-ide.org/
[5]: https://amaury.carrade.eu/
[6]: https://github.com/EtoileFilante
[pyconimg]: {static}/img/250-pycon-fr19.png
[rfranklin]: https://fr.wikipedia.org/wiki/Rosalind_Franklin
