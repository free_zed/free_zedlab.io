Title: Iaas (Interruption as a Sageness)
Date: 2021-11-12 19:00
Category: Bloc-notes
Status: published
Tags: talk, lyon, admin, incident, méthode, dev

Par [David Aparicio][author] [`1`][authorgl] [`2`][authorgh], organisé par [Café Devops][cafedevops] (via [Meetup][meetup]). Support (et sources) dispo sur [davidaparicio.gitlab.io][support].

> Père Castor, raconte nous une histoire (d'OPS)
> L'échec comme une source de connaissance. Pouvons-nous apprendre des erreurs des plus grands ?
>
> Nous allons, pour cela, faire la revue de quelques grands incidents de la dernière décennie : Github, Google, Amazon, Facebook, Apple, Microsoft, Gitlab ou plus récemment Fastly À travers la lecture des post-mortems des incidents, nous analyserons la root cause, la mise en place de la remédiation, et en extraire des bonnes pratiques.
>
> Notre speaker, David Aparicio, est ingénieur passionné en Informatique, diplômé INSA Lyon 2014, après deux années passées à UNICAMP au Brésil, il participe activement à la communauté, à travers des Meetups et des conférences. Sa devise: « Nul développeur n'est censé ignorer la sécurité »
> Si vous voulez également vous lancer comme speaker, voici ses [5 conseils pour soumettre son CfP](https://youtu.be/LWxe41DaONw)

---

Notes personnelles:

- _Post Mortem_ : base du talk
- Date du 1er bug de Grace Hooper: 1947
    - vrai insecte
    - innondation
- Elliot Alderson, Allsafe corp. : Drop table sur le mauvais env
    - Victimes: AWS, GitLab, Digital Ocean, OVHcloud,
    - Conseils:
        * Approbation manuelle/revue
        * Audit/protege
            - SIEM (analyse de loqs orienté sécu)
            - RBAC (Role Bse Access control)
        * credentials protégés (Vault, Keepass, etc.)
        * …
- David au bord de la piscine, tel d'astreinte sonne: incident sur le legacy
    1. Reboot: OK
    1. tel re-sonne
    1. analyse
    1. patch KISS: reload du service si pas avail en crontab, toujours en prod au bout d'un an (TPCM: Touche Pas C'est Magique)
- SPOF
    - Incident majeur 8 juin sur un grande quantité de service web
    - Origine: Fastly
    - CDN = SPOF
    - Promesse HTTP4 ou 5 de mieux exploiter les CDN
    - Conseil:
        - Personnaliser les message d'erreurs
        - Tester demande clients (E2E/Staging/Red-Black plate-forme)
        - Procedure du IT Road book
- OVH
    - Incident datalake legacy ->_flap sur zookeeper
    - ? Doubler la HIP de la JVM
    - ? Fine-tunning rde JVM
    - -> mise en cron d'une commande de nettoyage de la DB
- Blast effect : Zookeper, key-val store, base de beaucoup de système distribué, créé en même temps qu'Hadoop
- Conseils :
    - Protocole d'incident :
        - war room
        - doc collab
        - etc.
    - maintenir les version a jour des lib
    - circuit breaker
    - …
- Criteo
    - Conseils :
        - tests de perf
        - monito des KPI OS
        - Observabilité / sonde
        - Serveur de delestage
- NewsBlur : Mise en prod d'un MongoDB de dev
- Twitch : 130G de DB publié
    - Avoid HDD (Hype Driven Development)
    - CI/CD
    - DevSecOps: test auto de secu (admin:admin, )
    - etc.
- DNS :
    - Slack
    - FB
    - Conseils:
        - ne pas changer ses DNS pour 1.1.1.1 / 8.8.8.8 permanently
        - séparation des préoccupation
        - Ne pas se contenter de l'audit
        - etc.
- Split-brain
    - Github


Conclusion
==========

- SRE blameless culture
- QA / Chaos monkey
- Formation des équipes
- Tester les backups
- CI/CD avec devSecOps
- _Wheel of misfortune_


[author]: https://davidaparicio.gitlab.io/website/
[authorgh]: https://github.com/davidaparicio
[authorgl]: https://gitlab.com/davidaparicio
[cafedevops]: https://cafedevops.org/
[meetup]: https://www.meetup.com/fr-FR/cafe-devops-lyon/events/281097147/
[support]: https://davidaparicio.gitlab.io/website/talks/CafeDevOps2021_IaaS.pdf
