Title: Connaitre l'IP d'une VM VirtualBox
Date: 2019-05-13 11:42
Summary: Utilisation d'une VM VirtualBox en ligne de commande sur un hôte Debian
Category: Bloc-notes
Tags: debian, admin, cli, vm, virtualbox
Status: published
Lang: fr

Sur la machine invitée (VM)

```shell
apt install gcc make build-essential linux-headers-`uname -r`
wget https://download.virtualbox.org/virtualbox/6.0.8/VBoxGuestAdditions_6.0.8.iso
mount VBoxGuestAdditions_6.0.8.iso /mnt/ -t iso9660 -o loop
/mnt/VBoxLinuxAdditions.run
reboot
```

Sur la machine hôte (VirtualBox)

```shell
vboxmanage list vms
vboxmanage guestproperty enumerate vdeb
vboxmanage startvm vdeb --type headless
vboxmanage guestproperty enumerate vdeb
vboxmanage guestproperty get vdeb "/VirtualBox/GuestInfo/Net/0/V4/IP"
```

---

Sources consultées :

- [Installer les _additions invités_ dans VirtualBox][vbxguest] [fr]
- [Trouver l’IP d’une machine virtuelle VirtualBox][vbxip] [fr]
- [Installation de VirtualBox 4 en ligne de commande][vbxcliinstall] [en]
- [Utilisation de VirtualBox en lignes de commande][vbxclirun] [en]
- [VirtualBox 6 sur Debian 10 (buster)][vbx6] [en]
- [Installer les _additions invités_ dans VirtualBox en ligne de commande][vbxgestadd] [en]
- [download.virtualbox.org][vbxdl]
- [Download VirtualBox for Linux Hosts][vbxwiki] [en]
- [Controler VirtualBox avec `vboxmanage`][vbxclictrl]
- [Controler VirtualBox en mode `headless`][vbxclissh]

[vbxguest]: https://www.noobunbox.net/virtualisation/installer-les-additions-invites-virtualbox "Installer les Additions Invités Virtualbox, par Novakin"
[vbxip]: https://www.noobunbox.net/virtualisation/trouver-ip-machine-virtuelle "Trouver l’IP d’une machine virtuelle, par Novakin"
[vbxcliinstall]: https://www.lisenet.com/2014/virtualbox-4-1-on-a-headless-debian-wheezy-server/ "VirtualBox 4.1 on a Headless Debian Server, by Thomas"
[vbxclirun]: https://www.techrepublic.com/article/how-to-run-virtualbox-virtual-machines-from-the-command-line/ "How to run VirtualBox virtual machines from the command line, By Jack Wallen"
[vbx6]: https://tecadmin.net/install-virtualbox-on-debian-10-buster/ "How To Install VirtualBox 6.0 on Debian 10 (Buster), by Rahul"
[vbxgestadd]: https://unix.stackexchange.com/a/18926/280824 "How To Install Virtualbox Guest Additions On CentOS via Command Line only"
[vbxdl]: https://download.virtualbox.org/virtualbox/ "VirtualBox downloads"
[vbxwiki]: https://www.virtualbox.org/wiki/Linux_Downloads "Download VirtualBox for Linux Hosts"
[vbxclictrl]: http://www.ubuntugeek.com/how-to-control-virtual-machines-virtualbox-using-vboxmanage.html "How to control virtual machines (Virtualbox) using VBoxManage, by Ruchi"
[vbxclissh]: https://developers.caffeina.com/use-virtualbox-interface-headless-with-ssh-5552bf793d5f "Why use the boring VirtualBox interface? Use it headless with ssh!, by Gabriele Diener"
