Title: Histoires d'un S.R.E. perfectionniste sous pression
Category: Conférences
Date: 2022-09-29 19:00
Lang: fr
Status: published
Summary: Maintenir une infrastructure en conditions opérationnelles demande un certain nombre de choses, dont l'accès à des logs de la dites infrastructure.
Tags: afpy, talk, lyon, dev, cli, python, helloworld, logs, ovh, ldp


### Lire un flux Logs Data Plateform avec de bonnes pratiques python.

Cette présentation c’est déroulée dans le cadre des [rencontres lyonnaises et mensuelles de l'AFPy][afpy] organisées par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy]. (via [Meetup][meetup])

![logo AFPy Lyon][afpyimg]

> Maintenir une infrastructure en conditions opérationnelles demande un certain nombre de choses, dont l'accès à des logs de la dites infrastructure.

> Parmi les outils disponibles nous allons utiliser dans cette présentation le service [Logs Data Platform][ldp] qui utilise [OpenSearch][opnsrch] (entre autre).

> LDP (pour les intimes) est un service [OVHcloud][ovh] et cette présentation vous proposera de découvrir comment accéder à des logs LDP en utilisant Python.

- Support de présentation : [📦️ archive][support] / [📝 `ftalk/ldpy`][support-gitlab]
- Dépôt de code: [`forga/tool/ovh/ldpy`][repo]


[afpy]: https://www.afpy.org/
[afpyimg]: {static}/img/afpylyon-200.png
[cbouillon]: https://www.courtbouillon.org/
[ldp]: https://www.ovhcloud.com/fr/logs-data-platform/
[meetup]: https://www.meetup.com/fr-FR/python-afpy-lyon/events/287980312/
[opnsrch]: https://opensearch.org/
[ovh]: https://www.ovhcloud.com/fr/
[repo]: https://gitlab.com/forga/tool/ovh/ldpy
[stella]: https://stella.coop/
[support-gitlab]: https://gitlab.com/ftalk/ldpy/-/blob/afpy/PITCHME.pdf
[support]: {static}/pdf/20220929-afpy-ldpy.pdf
