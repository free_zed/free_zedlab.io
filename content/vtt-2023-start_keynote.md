Category: Bloc-notes
Date: 2023-02-02 09:30
Status: Published
Summary: Session d'ouverture du Very Tech Trip.
Tags: vtt, ovh, keynote, paris, business
Title: Keynote d'ouverture

Par _Octave Klaba, Thierry Souche, Laurent Berger, Pierre Lamarche, Yaniv Fdida_ - [**KEYNOTE** _Very Tech Trip_][vtt]

### Keynote #1

> 5 speakers passionnés par leur métier, tombés dans la marmite de la Tech dès leur plus jeune âge. Pour évoquer les thèmes du cloud native & move to cloud, d'infra, hardware & sustainable cloud, de data & IA, du web et partager leur vision de la souveraineté technologique et opérationnelle et de la sécurité.
>
> 1 heure pour faire le tour de la Tech, voilà leur défi.


---

Notes personnelles
==================

* [Octave Klaba][oles], intro et plan en 5 axes
    1. géographie
        - Ajout de zone: Paris 23, Canada 23, ALlemagne 24, US, Inde, Suisse,
    1. produits
        - public cloud [Laurent Berger](https://www.linkedin.com/in/lberger)
            * IAM, KMS, Metal cloud
        - [Thierry Souche][tsouche]
            * [ForePaas](¡https://www.forepaas.com)
        - [Lilian Lisanti](https://www.linkedin.com/in/lilianlisanti)
            * _lift and shift_
            * Dernière techno VMware (vSphere 8 arrive)
            * Nutanix: managed, packaged, BYOL
            * NetApp: EFS, chez PCC
            * SAD: Hardware & VMwane VSAN certifié SAP HANA
            * Backup: Veeam, Zerto
        - [Pierre Lamarche](https://www.linkedin.com/in/pierrelamarche)
            * intégration de git, OVH web statistic
            * mail: opensource zimbra, SNC
            * VOIP: nouvelle offre softphone, intégration ITSN & CMS
            * marque blanche: mail et VOIP
        - [Yaniv Fdida](https://www.linkedin.com/in/yaniv-fdida-876b153b)
            * plus de densité compute / storage / memory / GPU
            * de la beta pour client
            * immersion cooling
            * network: aggregation 4 lien 25G, vRack services Endpoint, IPv6 dans le vRack, DDoS monitoring
        - [Octave Klaba][oles], Hybrid Cloud
            * edge computing
            * verticale: healthcare, spatial, quantum
            * WTF: C'est la terre qui est inspirante

[oles]: https://www.linkedin.com/in/octave-klaba-3a0b3632
[tsouche]: https://www.linkedin.com/in/thierrysouche
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
