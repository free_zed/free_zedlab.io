Title: Histoires d'un salarié perfectionniste sous pression
Category: Conférences
Date: 2019-10-23 19:00
Status: published
Summary: Mes 7 premiers mois à la découverte de la vie professionnelle d'un développeur
Tags: afpy, talk, git, gitlab, devops, django, lyon, dev, admin, python


Démarrer une activité de développement dans une TPE est une aventure où tout est possible, mais quand, en plus, on est _junior_ les choix d'organisations et d'outils deviennent moins évident.

Dans cette présentation je reviens sur l'expérience de mes 7 premiers mois de développeur junior où j'ai essayé de ne pas me perdre entre le code, la documentation, les déploiements et la communication.

Cette [présentation publique][meetup] s'est déroulée dans le cadre des [rencontres  lyonnaises et mensuelles][afpy] de l'AFPy.

---

Les liens vers mes _bacs à sable_ évoqués durant la présentation :

- [Ansible](https://gitlab.com/free_zed/myasb)
- [Django](https://gitlab.com/free_zed/djbp)
- [PlantUML](https://gitlab.com/free_zed/mypumlt)

Le support est disponible en cliquant **sur le logo** de _l'AFPy Lyon_ ci-dessous.

[![logo AFPy Lyon][afpyimg]][support]


[afpy]: https://www.afpy.org/
[afpyimg]: {static}/img/afpylyon-200.png
[meetup]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/events/cqwghryznbnc/
[support]: https://gitpitch.com/free_zed/afpy19/master?grs=gitlab
