Title: Comment programmer un émulateur NES ?
Date: 2023-09-20 18:37
Summary: Découvrir une architecture possible d'un émulateur, et comment en implémenter un en nous focalisant sur le CPU, la mémoire, et les fameuses ROMs
Category: Bloc-notes
Tags: afpy, talk, lyon, dev, python, performance, diy
Status: Published

Par _Guillaume Roche_ [<sup>1</sup>][author-l]<sup>,</sup>[<sup>2</sup>][author-l] organisé par [CourtBouillon][cbouillon], [Genymobile][genymobile] et l'[AFPy][afpy] (via [Meetup][meetup]).

> Pour ce meetup de rentrée, Guillaume nous parle d’émulation, de simulation, de virtualisation… et d’un peu de C++ !
>
> Ces différents concepts permettent d'exécuter des programmes hors du cadre matériel et logiciel pour lequel ils ont été conçu. Si le cas d'usage le plus populaire aujourd'hui est le rétro-gaming, Il en existe une multitude. Il est d'ailleurs probable que vous utilisiez au quotidien ces technologies.
>
> Passionné par ces aspects, je me suis lancé le défi d'implémenter un émulateur d'une console NES. Pourquoi la NES ? C'est une machine relativement simple, extrêmement bien documentée, et de nombreux émulateurs open-source existants peuvent nous aider.
>
> Je vous propose de découvrir avec moi une architecture possible d'un émulateur, et comment en implémenter un en nous focalisant sur le CPU, la mémoire, et les fameuses ROMs. Nous évoquerons également comment rendre notre émulateur le plus fidèle possible. Enfin, nous verrons quelle stratégie de tests peuvent s’appliquer sur des projets d'émulateurs.

---

Notes personnelles
==================

* Définitions
    - Simulation? Réimplémenter le comportent d'un OS (WINE)
    - Émulation? Réimplémenter le hardware
    - Virtualisation? Émulation avec accès direct au hardware
* implementation
    - NES CPU : MOS 6502
    - les registres: un objet
    - Initialisation CPU
    - types d'instruction
    - le code `C++` de cet émulateur : [`guillaumeroche/Nestor`](https://gitlab.com/guillaumeroche/Nestor)
* Précision de l'émulation
    - partit pris on aurait pu vouloir ciler la perf
* ressources
    - [Nintendo Entertainment System Documentation](https://www.nesdev.org/NESDoc.pdf)
    - [NMOS 6502 Opcodes](http://www.6502.org/tutorials/6502opcodes.html), by John Pickens, Updated by Bruce Clark and by Ed Spittles
    - [Nesdev Wiki](https://www.nesdev.org/wiki/Nesdev_Wiki), the source for all your NES programming needs
    - [Vidéo]: [NES Architecture Explained](https://www.youtube.com/watch?v=PwZEBE66an0)
    - [NES online code tester] : [Easy 6502](https://skilldrick.github.io/easy6502/)
    - [NES online code tester] : [8 bit workshop](http://8bitworkshop.com/)


[afpy]: https://www.afpy.org/
[author-h]: https://github.com/guillaume-roche
[author-l]: https://gitlab.com/guillaumeroche/
[cbouillon]: https://www.courtbouillon.org/
[meetup]: https://www.meetup.com/python-afpy-lyon/events/295796925/
[genymobile]: https://www.genymobile.com/
[support]: https://
