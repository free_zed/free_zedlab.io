Title: Développeur à 50 ans
Date: 2022-10-19 19:00
Lang: fr
Status: Published
Summary: Peut on avoir commencé à coder avant ce siècle et continuer à occuper un poste dans l'IT après 50 ans?
Category: Bloc-notes
Tags: afpy, talk, lyon, dev

Par [Benjamin Marron][author], organisé par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy]. (via [Meetup][meetup]).

### Développeur à 50 ans

> Peut on avoir commencé à coder avant ce siècle et continuer à occuper un poste dans l'IT après 50 ans ?
>
> Basé sur une histoire vraie, ce talk retrace mon parcours professionnel et ma manière de suivre l’évolution des métiers dans la tech et le développement lors de la dernière décennie.

---

Notes personnelles
==================

* vieux ou pas vieux?
    - crise de milieu de vie
    - bilan de compétence
    - Devoxx
    - Pyramide des ages: 2015 quarantenaire 11%
* Choix de carrière
    - organisation hérité de l'industrie
    - chemin de carrière unique
    - contrainte difficile a vivre
* La formation
    - de nouveaux collègues
    - de nouveaux métiers
    - mais pour chaque nouvelles semaines
* Retour aux sources
    - Agilité  c'est sympa
    - les PME/PMI aussi
    - des nouvelles voies de carrière (lead / senior/ devrel)
    - des nouvelles manières de vendre du service
* Pyramide des ages 2022
    - quarantenaire 22%
    - cinquantenaire 8%
* [enfrasys](https://www.enfrasys.fr/)
    - électricien, automaticiens
    - tunnel, ferroviaire



[afpy]: https://www.afpy.org/
[author]: https://twitter.com/bmarron
[cbouillon]: https://www.courtbouillon.org/
[meetup]: https://www.meetup.com/python-afpy-lyon/events/288986395/
[stella]: https://stella.coop/
