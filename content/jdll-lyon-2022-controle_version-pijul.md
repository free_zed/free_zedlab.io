Title: Pijul, contrôle de version et théorie des patchs
Date: 2022-04-03, 16:00
Summary: Pijul est un système de contrôle de versions basé sur une théorie mathématique des changements.
Category: Bloc-notes
Tags: talk, jdll, lyon, git, mercurial, innovation, performance, rust, citoyen
Status: Published
Lang: fr

### [Pijul, contrôle de version et théorie des patchs](https://pretalx.jdll.org/jdll2022/talk/M3GRXV/)

par [Pierre-Étienne Meunier][pe_meunier] - 2022-04-03 16:00–16:55 Studio danse

![logo JDLL 2022]({static}/img/jdll-2022.jpg)

[Pijul][pijul] est un système de contrôle de versions basé sur une théorie mathématique des changements.

Le contrôle de versions est un outil fondamental du développement. Or, les outils existants (Git, SVN, Mercurial…) sont d'une telle complexité que de nombreux autres domaines du travail informatique ne peuvent pas l'utiliser. De plus, cette complexité crée des processus rigides et des tâches inutiles, ce qui gaspille un temps d'ingénierie conséquent à l'échelle globale.

[Pijul][pijul] prétend résoudre une partie de ces problèmes en utilisant une théorie mathématique solide, tout en étant totalement transparente pour l'utilisateur. En particulier, la commutation de patchs permet une utilisation intuitive et un passage à de très grandes échelles.

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]

---

Notes personnelles:

- Système distribué
- conflit
    - algèbre a la rescousse
        - associativité
        - commutativité
    - Le problème de la fusion a 3 branches ([voir][fusion_3])
- Quelles structure de donnée?
    - Proposition de [Samuel Mimran][s_mimran] (polytechnique)
        - modélisation en graph
- Définition de conflit
    - sommet
- En pratique
    - en rust
    - beta depuis 18/1/22
    - [`sanakirja`](https://docs.rs/sanakirja/latest/sanakirja/): lib structure de données transactionnelle
    - [`libpijul`](https://docs.rs/crate/libpijul/1.0.0-beta.1): les algo de cet présentation
    - [`pijul`](https://nest.pijul.com/pijul/pijul): ligne de commande et réseau
    - [`trussh`](https://docs.rs/thrussh/latest/thrussh/): implémentation SSH client et serveur rust
- hébergement: [`nest.pijul.com`][nest]
    - -> [générateur de page de manuel git](https://git-man-page-generator.lokaltog.net/#ZXhwbG9kZSQkc3RhZ2U=)
- Bonus
    - _cherry-picking_, clones, partiels, [commutation](https://pijul.org/manual/why_pijul.html#change-commutation)
    - patch détachable
    - peut fonctionner en lecture seule


[captation]: https://www.videos-libr.es/w/5obLQ8WGTScXyCMSHTDFsK
[pijul]: https://pijul.org/
[pe_meunier]:https://fr.linkedin.com/in/pierre-%C3%A9tienne-meunier-1b93b619b
[fusion_3]: https://pijul.org/manual/why_pijul.html#associativity
[nest]: https://nest.pijul.com
[s_mimran]: https://fr.linkedin.com/in/smimram
