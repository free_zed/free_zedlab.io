Title: Histoires d'un sysadmin perfectionniste sous pression
Date: 2022-04-02 13:00
Category: Conférences
Lang: fr
Summary: Découvrez ZFS: un stockage fiable, puissant et accessible.
Status: Published
Tags: jdll, talk, lyon, storage, zfs, admin, cli, backup


### Découvrez `ZFS`: un stockage fiable, puissant et accessible.

Les 2 & 3 avril 2022 se sont déroulées les [Journées du Logiciel Libre][jdll] à Lyon.

[![logo JDLL 2022]({static}/img/jdll-2022.jpg)](https://pretalx.jdll.org/jdll2022/talk/AHEQRE/)

J'y ai présenté [ZFS][openzfs] un système de fichier qui est aussi un gestionnaire de volumes.

> `ZFS` est un outil passionnant qui va au-delà d'un système de fichier. Cette présentation se veut techniquement accessible et vise à partager ma découverte de cet outil pour (peut-être) vous donner envie de l'essayer.

> Né au début des années 2000 au sein de _Sun Microsytems_, ZFS est aujourd'hui développé au travers du projet [openZFS][openzfs] pour les noyaux _Linux_ et _freeBSD_.

![openzfs logo]({static}/img/openzfs.png)

Support de présentation : [📦️ archive]({static}/pdf/20220402-jdll-openzfs.pdf) / [📝 `ftalk/openzfs`](https://lab.frogg.it/ftalk/openzfs/-/blob/jdll/pdf/20220402-jdll-openzfs.pdf)

_[MàJ du 3/10/22]:_ [Lien vers la captation vidéo][captation]


[jdll]: https://jdll.org
[openzfs]: https://openzfs.org
[captation]: https://www.videos-libr.es/w/tnRUMm49tErvDhAT97pP2F
