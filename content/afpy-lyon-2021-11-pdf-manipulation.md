Title: Manipuler des PDF en Python
Category: Bloc-notes
Date: 2021-11-25 19:00
Status: published
Summary: Tour des outils python permettant de manipuler du PDF dans différentes situations.
Tags: talk, afpy, lyon, pdf, dev, python, cli


Par [Guillaume Ayoub][author] (<sup>[1][authorgl], [2][authorgh]</sup>), organisé par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy].  (via [Meetup][meetup]).

_**Support**: sur [`courtbouillon.org`][support] ou [archive][support-local]_

> Il existe de nombreuses bibliothèques pour créer et modifier des documents PDF en Python, et il est parfois difficile de savoir quel outil choisir. Comme souvent, la réponse dépend de ce que l’on veut faire !
>
> Nous ferons un petit tour des différents besoins que vous pourriez avoir concernant la manipulation des PDF, que ce soit pour créer des documents simples ou complexes, apporter des modifications à des PDF existants, ou accéder à certaines informations de leur contenu. Et bien sûr, nous découvrirons également des outils adaptés à la résolution de ces problèmes.
>
> Si vous cherchez l’inspiration pour automatiser certaines tâches (pas particulièrement joyeuses…) que vous faites à la main sur vos documents, vous pourriez découvrir deux ou trois solutions qui pourraient bien vous faire gagner du temps !

---

Notes personnelles
==================

1. Présentation basique de la structure d'un PDF
1. Lib de récup python
    - [pdfminer](https://readthedocs.org/projects/pdfminer-docs/downloads/pdf/latest/): récupération de texte
    - [PyPDF2][pypdf2]; récupération de métadonnées
1. Lib d'affichage GPL/AGPL
    - [Ghostscript][ghostscript]
    - [Poppler][poppler] (ex xpdf):
1. Transformer en image
    - binding python [Poppler][poppler]
    - binding python [Ghostscript][ghostscript] pas super fiable
1. Modifier un PDF en python
    - Rotation: [pdfrw](https://pypi.org/project/pdfrw/)
    - Fusion 2 PDF: [PyPDF2][pypdf2]
1. Créer un PDF
    - Manuellement : difficile, long et maitrise de la spec nécessaire. Résultat image vectorielle. Ex. [pydyf](https://www.courtbouillon.org/pydyf)
    - interface haut niveau: liberté et faciliter une mise en page basique. Ex. [borb](https://pypi.org/project/borb/) (basique et récente), [reportlab](https://pypi.org/project/reportlab/) (historique et plus complète). Double licence libre/payant. Emprisonnement haut niveau du code python
    - depuis HTML/CSS: Mise en page complexe, standards solides, pas précis au pixel. Ex. [Weasyprint](https://weasyprint.org/)

[afpy]: https://www.afpy.org/
[authorgh]: https://github.com/liZe
[authorgl]: https://www.afpy.org/
[author]: https://yabz.fr/
[ghostscript]: https://www.ghostscript.com/
[meetup]: https://www.meetup.com/fr-FR/Python-AFPY-Lyon/events/jkcfnryccpbgc/
[poppler]: https://gitlab.freedesktop.org/poppler/poppler
[pypdf2]: https://pypdf2.readthedocs.io/en/latest/index.html
[support]: https://www.courtbouillon.org/static/presentations/Manipuler%20des%20PDF%20en%20Python.pdf
[support-local]: {attach}/pdf/20211125-manipuler_des_pdf_en_python.pdf
[cbouillon]: https://www.courtbouillon.org/
[stella]: https://stella.coop/
