Title: Clôture du samedi
Date: 2023-02-18 17:09
Summary:
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, keynote,
Status: published

Par **[Marc Debureaux][mdebureaux]** - Salle [Alfred Wegener][awegener]


### [Clôture du samedi][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Session de clôture de la journée du samedi.

---

Notes personnelles
==================

* Récupération des tours de cou en fin d'évenement, pour lavage réutilisation
* Appel
    - aux volontaires pour rejoindre l'association
    - aux futurs site pour recevoir une prochaine éditions
    - aux rassemblements locaux
    - aux
* 400 membres AFPy à ce jours
* 17-21 juillet: [EuroPyton Prague](https://europython.eu)
    - Participation payante
    - Bourse possible
    - CFP: 6 mars


[abstract]: https://www.pycon.fr/2023/fr/talks/plenary.html#cloture-du-samedi-saturday-clo
[awegener]: https://fr.wikipedia.org/wiki/Alfred_Wegener
[mdebureaux]: https://fr.linkedin.com/in/mdebnet
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
