Title: CoWorks : créer des microservices en utilisant Flask/AWS Lamba et Airflow
Date: 2023-04-27 19:00
Summary:
Category: Bloc-notes
Tags: afpy, talk, lyon, dev, python, aws, aws-lambda, serverless, web, flask, micro-services, performance
Status: Published

Par [Guillaume Doumenc][author], organisé par [CourtBouillon][cbouillon], [Stella][stella] et l'[AFPy][afpy] (via [Meetup][meetup]).

> [Guillaume][author] vient nous présenter [CoWorks][coworks], un framework unifié de microservices serverless basé sur les technologies AWS (API Gateway, AWS Lambda), le framework Flask (Flask/Click) et la plateforme Airflow.
>
> Dans cette présentation, nous verrons :
>
> * un aperçu des concepts : tech vs business micro-services
> * les avantages du framework : modularité, maintenance, capacité de mise à l'échelle
> * un retour d'expérience d'un client − NeoRezo

---

Notes personnelles
==================

- 123 imprim il y a 10 ans
    * Django
- [NeoRezo][neorezo]
    * Web2print marketplace provider
    * intranet eshop pour matériel de communication d'entreprise multisite
    * Toujours Django
    * Intérêt dans le produits AWS lambda
        - Framework kalis (?)
            * mais pas de terraform
            * pas d'asynchronisme
- Création de [Coworks][coworks] pour utiliser Python dans AWS Lambda en microservice
- Technologies
    * Pas de nouveauté, utiliser les tech au fond, avant de changer
    * AWS: API Gateway + Lambda
    * [Terraform](https://www.terraform.io/): génère une infra chez AWS ou autre provider / environnement
    * [Airflow](https://airflow.apache.org/) author, schedule and monitor workflows.
    * [Flask](https://flask.palletsprojects.com/)
    * 2 niveaux de services
        - synchrone / backend
        - asynchrone / métier
- Tech microservices
    * Stateless
    * Full flask
        - toute les extensions
- Biz microservice
    * statefull
    * 4 taches
        1. transform
        1. call
        1. listen
        1. read
- Usage feedback
    * catalog complexe
    * grande disparité de produits/clients
    * ~100 tech MS
        - ~50 Biz MS
    * ~25 déploiements / jour
- Bilan
    * positif: agilité, modularité, processus parametrable
    * Négatif: Performance
- misc
    * [Astronomer](https://www.astronomer.io/)


[afpy]: https://www.afpy.org/
[author]: https://fr.linkedin.com/in/gdoumenc
[cbouillon]: https://www.courtbouillon.org/
[coworks]: https://github.com/gdoumenc/coworks
[meetup]: https://www.meetup.com/python-afpy-lyon/events/292447608/
[neorezo]: https://neorezo.io/
[stella]: https://stella.coop/
