Category: Bloc-notes
Date: 2023-02-02 16:50
Status: published
Summary: Un petit projet qui a commencé un été sans aucune ambition, pour aider ma guilde à mieux gérer la fabrication d'objets en équipe dans un jeu en ligne
Tags: vtt, ovh, talk, paris, gaming, collectif, dev, logiciel-libre, gui,
Title: De la première ligne de code au succès : REX d’un projet open source

Par [Flavien Normand][author] - [**TALK** _Very Tech Trip_][vtt]


### De la première ligne de code au succès : REX d’un projet open source

> Je vous propose un REX sur le projet [FFXIV Teamcraft](https://ffxivteamcraft.com/), un petit projet qui a commencé un été sans aucune ambition, pour aider ma guilde à mieux gérer la fabrication d'objets en équipe dans un jeu en ligne. Aujourd'hui, il est traduit en 10 langues et utilisé par des milliers de personnes dans le monde, plus de 200 000 utilisateurs actifs mensuel, le tout Open Source, avec les moyens du bord.
>
> Cette présentation raconte l'histoire derrière ce projet, les problèmes rencontrés, les risques (techniques comme autres), elle détaille aussi tout ce que j'ai pu y gagner, et y perdre. C'est également l'occasion de parler de la communauté, de l'impact que celle-ci a, des rencontres, des échanges, et de tout ce qui a pu être appris.


---

Notes personnelles
==================

* [Teamcraft](https://github.com/ffxiv-teamcraft)
* Final fantaisy 14: fabrication d'objets
    - recette imbriqué et deviennent vite complexe
* Stack: firebase, angular
* Chiffres
    - Création 15/8/17
    - 400000 actifs mensuels
    - 4M de liste créée
    - 9995 commit
    - 850 github stars
    - 10 langues: par [crowdin](https://crowdin.com/)
    - …
* Devel
    - Creation: Angular + material design
    - amélioration
    - refactoring complet: andesign (design system alibaba)
    - arrivé d'electron: fonctionnalité superposable au jeu
    - `nmap`: lire les flux réseaux. Pas de lecture mémoire
    - activité
* Financement
    - Patreon (crowdfunding)
    - playwire (regie pub)
    - jour mis a dispo pour open source par employeur (zenika)
* Avantage
    - Grosse communauté internationnale
    - bac a sable perso
    - projet vitrine efficace CV
* Inconvéniant
    - très peu de contact avec Square Enix
    - beaucoup de temps passé (~6000h / 2-4h/j )
    - beaucoup de stress
    - confusion projet OSS / produit pro avec les utilisateurs

[author]: https://https://www.linkedin.com/in/flavien-normand-908171101
[vtt]: https://verytechtrip.ovhcloud.com/fr/very-tech-trip/sessions/
