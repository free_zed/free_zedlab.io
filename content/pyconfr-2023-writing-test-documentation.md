Title: Writing Great Test Documentation
Date: 2023-02-18 14:00
Summary: SUMMARY
Category: Bloc-notes
Tags: live-notes, pyconfr, talk, bordeaux, python, méthode, dev
Status: published

Par **[David Andersson][author]** - Salle [Henri Poincaré][hpoincare]

### [Writing Great Test Documentation][abstract]

[![logo PyConFr Bordeaux 2023][pyconimg]][pyconfr]

> Have you ever needed to understand a new project and started reading the tests only to find that you have no idea what the tests are doing? In this talk we will discuss how to write great test documentation to make this a thing of the past! Writing great test documentation as you are writing tests will improve your tests and help you and others reading the tests later. We will first look at why test documentation is important both when writing tests and for future readers, then look at a framework that helps give some structure to your test documentation. Next, we will look at a showcase of the flake8-test-docs tool that automates test documentation checks to ensure your documentation is great! Finally we briefly discuss how this framework would apply in more advanced cases, such as when you are using fixtures or parametrising tests.

_[Support][support]_

---

Notes personnelles
==================

* Presentation
    - Travailpour Cannonical
* Pourquoi documenter ses test?
    - démarche similaire au TDD
    - exprimer l'intention avant le code
* Documenter les étapes: prérequis, objectif d'execution et résultat attendus
* Example
    - utiliser les docstring et documenter en 3 paragraphes reprenant les 3 étapes:
        1. prérequis
        1. execution souhaité
        1. résultat attendu
* Requirements
* Formaliser la structure de la documentation
    - nommer le paragraphes
    - indenter quand le pragraphe continue
* Automatiser
    - `flake8-test-docs`: utilisé depuis plusieurs mois
        * docstring manquant
        * paragraphe manquant


[abstract]: https://www.pycon.fr/2023/fr/talks/30m.html#writing-great-test-documentati
[author]: https://jdkandersson.com
[hpoincare]: https://fr.wikipedia.org/wiki/Henri_Poincaré
[pyconfr]: https://www.pycon.fr/2023/
[pyconimg]: {static}/img/200-pycon-fr-23.png
[support]: https://
