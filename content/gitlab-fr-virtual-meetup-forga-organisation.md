Title: Histoires d'un collègue perfectionniste sous pression
Date: 2020-10-07 19:00
Category: Conférences
Status: published
Summary: S'organiser à petite échelle avec GitLab
Tags: forga, talk, git, gitlab, devops, django, dev, admin, python, packaging, pip, ci, cd, dry, agile, méthode, collectif, devops, statique, gitlab-pages, web, mkdocs

S'organiser à petite échelle avec GitLab
----------------------------------------

Une expérience d'organisation inspirée par la lecture (partielle) du [Handbook GitLab](https://about.gitlab.com/handbook/).

Un cas d'usage de GitLab qui concerne les (très) petites équipes/organisations.
À base de [sous-groupes](https://docs.gitlab.com/ce/user/group/subgroups/), [CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/), [`pages`](https://about.gitlab.com/stages-devops-lifecycle/pages/), documentation unique, [`pip`](https://pip.pypa.io/en/stable/) et [Django](https://djangoproject.com).

Dans cette présentation je reviens sur l'expérience de mes derniers mois de développeur solo accueillant un nouveau collègue.

Cette [présentation à distance et publique][meetup] c'est déroulée dans le cadre des [rencontres GitLab Francophones][meetupglfr].

---

Le support est disponible en cliquant **sur le logo** _GitLab Virtual Meetup_ ci-dessous.

[![logo GitLab][virtualmeetup]][support]

---

La présentation a été enregistrée et est disponible ici : [youtu.be/urvBh7-4RFo](https://www.youtube.com/watch?v=urvBh7-4RFo)

Les liens évoqués sont listés chronologiquement :

1. [Code de conduite GitLab](https://about.gitlab.com/community/contribute/code-of-conduct/)
1. [_All remote guide_](https://about.gitlab.com/company/culture/all-remote/guide/)
1. [Livre blanc _travail à distance_](https://about.gitlab.com/resources/ebook-remote-playbook/)
1. [Handbook GitLab : ~8500 pages](https://about.gitlab.com/handbook/about/#count-handbook-pages)
1. [_Sid's fix typo_](www.linkedin.com/posts/derfabianpeter_opensource-activity-6717786219301892097-jr6U)
1. [_Packages & Registries_](https://docs.gitlab.com/ee/user/packages/)
1. [Livraison de GitLab](https://about.gitlab.com/releases/)




[afpy]: https://www.afpy.org/
[afpyimg]: {static}/img/gitlab-200.png
[meetup]: https://www.meetup.com/fr-FR/GitLab-Meetup-France/events/273510158/
[support]: https://gitpitch.com/ftalk/2020-gitlab/stable?grs=gitlab
[virtualmeetup]: {static}/img/gitlab-virtual-meetup-s.png
[meetupglfr]: https://www.meetup.com/fr-FR/GitLab-Meetup-France/
